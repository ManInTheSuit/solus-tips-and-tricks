# Solus tips and tricks

Here you can find various tips and tricks that can make your Solus experience better.

Feel free to contribute with improvements if they aren't present.